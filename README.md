# Nightly Builds of Alpine 3.16

This repository only contains a CI/CD pipeline to build and deploy new Alpine Base v3.16 Docker images on a daily basis to Docker HUB.

The main reason for this project is, that many images on Docker HUB are outdated and bring a lot of CVEs with them.

You can test it yourself
```
docker run ghcr.io/aquasecurity/trivy:latest image procinger/alpine:latest
docker scan procinger/alpine:latest
```

At the moment only x86-64 & aarch64 images are built.
